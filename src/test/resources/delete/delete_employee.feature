Feature: Employee can be deleted

  Background:
    Given user is on the Login page
    When user enters valid credentials
    And user initiates employee creation

  @All
  @Delete
  Scenario Outline:
    When user with <Firstname>, <Lastname>, <StartDate> and <Email> is created
    And user with <Firstname> and <Lastname> is deleted
    And user initiates log out
    And user enters valid credentials
    Then user with <Firstname> and <Lastname> does not exist
    Examples:
      |Firstname|Lastname|StartDate|Email|
      |John    |Doe   |1933-06-23|john.doe@dasauto.com|