Feature: Employee data can be updated

  Background:
    Given Login page is opened
    When user enters valid credentials
    And user initiates employee creation
    And user with Boomba, Woomba, 2015-02-02 and boomba.woomba@somewhere.com is created

    @All
    @Edit
  Scenario Outline:
    When user with Boomba and Woomba is selected for update
    And user enters <Firstname> as first name
    And user enters <Lastname> as last name
    And user enters <StartDate> as start date
    And user enters <Email> as email
    And user updates employee
    And user initiates log out
    And user enters valid credentials
    Then user with Boomba and Woomba does not exist
    And user with <Firstname> and <Lastname> exists
    Examples:
      |Firstname|Lastname|StartDate|Email|
      |Mister   |Twister |2017-20-03|mister.twister@yahoo.com|