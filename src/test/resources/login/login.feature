Feature: User can login with valid credentials only

  Background:
    Given Login page is opened

  @All
  @Login
  Scenario: User is able to log on using valid credentials
    When user enters valid credentials
    Then user is on the Employee page

  @All
  @Login
  Scenario Outline: User is unable to login with invalid credentials
    When user enters <Username> as login
    And user enters <Password> as password
    Then user is on the Login page
    Examples:
      |Username|Password|
      |Dart    |Wader   |
      |Luke    |        |
      |        |Skywalker|
      |        |         |

  @All
  @Login
  Scenario: User can log out
    When user enters valid credentials
    Then user is on the Employee page
    When user initiates log out
    Then user is on the Login page