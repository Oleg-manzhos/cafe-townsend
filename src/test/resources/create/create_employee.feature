Feature: Employee could be created

  Background:
    Given user is on the Login page
    When user enters valid credentials
    And user initiates employee creation

  @All
  @Create
  Scenario Outline: Employee can be created if all fileds are populated
    When user enters <Firstname> as first name
    And user enters <Lastname> as last name
    And user enters <StartDate> as start date
    And user enters <Email> as email
    And user submits employee
    Then user is on the Employee page
    And user with <Firstname> and <Lastname> exists
    Examples:
      |Firstname|Lastname|StartDate|Email|
      |David    |Nolan   |1933-06-23|david.golang@dasenee.com|

#  @All
#  @Create
#  Scenario Outline: Employee can't be created if some field isn't populated
#    When user enters <Firstname> as first name
#    And user enters <Lastname> as last name
#    And user enters <StartDate> as start date
#    And user enters <Email> as email
#    And user submits employee
#    Then user is on the Employee creation page
#    Examples:
#      |Firstname|Lastname|StartDate|Email|
#      |         |Noffman |2003-05-07|test@nephew.nl|
#      |Berry    |        |2015-08-16|voorn@feurn.com|
#      |Dory     |Mory    |          |dory.mory@nfblm.co.uk|
#      |Every    |People  |2000-01-01|                     |