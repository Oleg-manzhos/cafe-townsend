package me.manzhos.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/", "json:target/cucumber.json"},
        features = {"src\\test\\resources"},
        glue = {"me.manzhos.definitions"},
        tags = {"@All"})
public class AllTestsRunner {
}
