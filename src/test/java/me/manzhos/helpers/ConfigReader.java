package me.manzhos.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    private File credentials = new File("credentials.properties");
    private File environment = new File("environment.properties");
    private Properties properties = new Properties();

    public String getEnvironmentURL(){

        String envValue = "";

        try {
            FileInputStream fileInput = new FileInputStream(environment);
            properties.load(fileInput);
            envValue = properties.getProperty("env");
        }
        catch (IOException ex){
            ex.printStackTrace();
        }

        return properties.getProperty(envValue);
    }

    public String getLogin(String loginProperty) {
        try {
            FileInputStream fileInput = new FileInputStream(credentials);
            properties.load(fileInput);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return properties.getProperty(loginProperty);
    }

    public String getPassword(String passwordProperty) {
        try {
            FileInputStream fileInput = new FileInputStream(credentials);
            properties.load(fileInput);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return properties.getProperty(passwordProperty);
    }
}

