package me.manzhos.definitions;

import cucumber.api.java.en.When;
import me.manzhos.pages.EditEmployeePage;

public class EditEmployeePageStepDefinition {

    EditEmployeePage editEmployeePage = new EditEmployeePage();

    @When("user updates employee")
    public void userUpdatesEmployee(){
        editEmployeePage.clickUpdateButton();
    }
}
