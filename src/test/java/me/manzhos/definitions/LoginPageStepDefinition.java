package me.manzhos.definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import me.manzhos.pages.LoginPage;
import org.junit.Assert;

public class LoginPageStepDefinition {

    private LoginPage loginPage = new LoginPage();

    @Given("^Login page is opened$")
    public void loginPageisOpened() {
        loginPage.openLoginPage();
    }

    @When("^user enters valid credentials$")
    public void validCredentialsAreEntered(){
        loginPage.setValidCredentials("admin.login", "admin.password");
    }

    @When("^user enters ([^\"]*) as login$")
    public void userEntersUsernameAsLogin(String login) {
        loginPage.setLogin(login);
    }

    @When("^user enters ([^\"]*) as password$")
    public void userEntersPassword(String password) {
        loginPage.setPassword(password);
    }

    @Then("^user is on the Login page$")
    public void userIsOnTheLoginPage() {
        loginPage.openLoginPage();
        Assert.assertTrue(loginPage.isLoginPage());
    }
}
