package me.manzhos.definitions;

import com.codeborne.selenide.Condition;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import me.manzhos.pages.EmployeePage;
import org.junit.Assert;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;

public class EmployeePageStepDefinition {

    private EmployeePage employeePage = new EmployeePage();

    @When("^user initiates log out$")
    public void initiateLogOut(){
        employeePage.initiateLogOut();
    }

    @Then("^user is on the Employee page$")
    public void userIsOnEmployeePage(){
        Assert.assertTrue(employeePage.isEmployeePage().isDisplayed());
    }

    @When("^user initiates employee creation$")
    public void userInitiateEmployeeCreation() {
        employeePage.initiateEmployeeCreation();
    }

    @When("^user with ([^\"]*) and ([^\"]*) is deleted$")
    public void deleteUser(String fName, String lName){
        employeePage.deleteUserByNames(fName, lName);
    }

    @When("^user with ([^\"]*) and ([^\"]*) is selected for update$")
    public void userIsSelectedForUpdate(String fname, String lname){
        employeePage.editEmployeeRecordByEditButton(fname, lname);
    }

    @Then("^user with ([^\"]*) and ([^\"]*) does not exist$")
    public void userWithGivenDataDoesntExist(String fname, String lname){
        employeePage.returnUserByName(fname, lname).shouldNot(visible);
    }

    @Then("^user with ([^\"]*) and ([^\"]*) exists$")
    public void userWithGivenDataExists(String fname, String lname){
        employeePage.returnUserByName(fname, lname).shouldBe(visible);
    }
}
