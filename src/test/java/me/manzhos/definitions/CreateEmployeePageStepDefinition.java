package me.manzhos.definitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import me.manzhos.pages.CreateEmployeePage;
import me.manzhos.pages.CreateUpdateCommonElementsPage;
import org.junit.Assert;

public class CreateEmployeePageStepDefinition {

    private CreateEmployeePage createEmployeePage = new CreateEmployeePage();
    private CreateUpdateCommonElementsPage common = new CreateUpdateCommonElementsPage();

    @When("^user submits employee$")
    public void userSubmitsEmployee(){
        createEmployeePage.submitEmployee();
    }

    @When("user with ([^\"]*), ([^\"]*), ([^\"]*) and ([^\"]*) is created")
    public void userIsCreatedWithGivenData(String fname, String lname, String sDate, String email){
        common.setFirstName(fname);
        common.setLastName(lname);
        common.setStartDate(sDate);
        common.setEmail(email);
        createEmployeePage.submitEmployee();
    }

    @Then("^user is on the Employee creation page$")
    public void userIsOnTheEmployeeCreationPage(){
        Assert.assertTrue(createEmployeePage.isCreateEmployeePage());
    }
}
