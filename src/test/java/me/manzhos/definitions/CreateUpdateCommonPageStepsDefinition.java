package me.manzhos.definitions;

import cucumber.api.java.en.When;
import me.manzhos.pages.CreateUpdateCommonElementsPage;

public class CreateUpdateCommonPageStepsDefinition {

    private CreateUpdateCommonElementsPage common = new CreateUpdateCommonElementsPage();

    @When("^user enters ([^\"]*) as first name$")
    public void userEntersFirstName(String firstName){
        common.setFirstName(firstName);
    }

    @When("^user enters ([^\"]*) as last name$")
    public void userEntersLastName(String lastName) {
        common.setLastName(lastName);
    }

    @When("^user enters ([^\"]*) as start date$")
    public void userEntersStartDate(String startDate) {
        common.setStartDate(startDate);
    }

    @When("^user enters ([^\"]*) as email$")
    public void userEntersEmail(String email) {
        common.setEmail(email);
    }
}
