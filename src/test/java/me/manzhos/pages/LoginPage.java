package me.manzhos.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import me.manzhos.helpers.ConfigReader;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage extends BasePage{

    private By loginFld = By.xpath("//span[contains(text(),'Username')]/following-sibling::input");
    private By passwordFld = By.xpath("//span[contains(text(),'Password')]/following-sibling::input");
    private By loginBtn = By.cssSelector("button.main-button");

    public Boolean isLoginPage(){
        return WebDriverRunner.getWebDriver().getCurrentUrl().endsWith("login");
    }

    public void setValidCredentials(String loginProperty, String passwordProperty){
        setLogin(new ConfigReader().getLogin(loginProperty));
        setPassword(new ConfigReader().getLogin(passwordProperty));
        clickLogin();
    }

    public void openLoginPage() {
        baseUrl = getURL();
        open(baseUrl);
    }

    public void setLogin(String login){
        $(loginFld).clear();
        $(loginFld).sendKeys(login);
    }

    public void setPassword(String password){
        $(passwordFld).clear();
        $(passwordFld).sendKeys(password);
    }

    private void clickLogin(){
        $(loginBtn).click();
    }
}
