package me.manzhos.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import me.manzhos.helpers.ConfigReader;
import org.junit.BeforeClass;

public class BasePage {

    @BeforeClass
    public void setConfiguration(){
        System.out.println("I see ths hook, it is Before");
        Configuration.startMaximized = true;
        Configuration.timeout = 10000;
        Configuration.baseUrl = getURL();
    }

    @After
    public void tearDown() {
        System.out.println("I see ths hook, it is After");
        WebDriverRunner.closeWebDriver();
    }

    public String getURL(){
        return new ConfigReader().getEnvironmentURL();
    }
}
