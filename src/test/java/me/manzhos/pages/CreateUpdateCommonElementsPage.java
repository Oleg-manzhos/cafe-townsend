package me.manzhos.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CreateUpdateCommonElementsPage {

    private By firstName = By.xpath("//span[contains(text(),'First name')]/following-sibling::input");
    private By lastName = By.xpath("//span[contains(text(),'Last name')]/following-sibling::input");
    private By startDate = By.xpath("//span[contains(text(),'Start date')]/following-sibling::input");
    private By email = By.xpath("//span[contains(text(),'Email')]/following-sibling::input");

    public void setFirstName(String fName) {
        $(firstName).clear();
        $(firstName).sendKeys(fName);
    }

    public void setLastName(String lName) {
        $(lastName).clear();
        $(lastName).sendKeys(lName);
    }

    public void setStartDate(String sDate) {
        $(startDate).click();
        $(startDate).clear();
        $(startDate).sendKeys(sDate);
    }

    public void setEmail(String emailAddr) {
        $(email).clear();
        $(email).sendKeys(emailAddr);
    }
}
