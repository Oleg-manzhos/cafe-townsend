package me.manzhos.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EditEmployeePage extends BasePage {

    private By updateBtn = By.xpath("//button[text()='Update']");
//    private By deleteBtn = By.xpath("//p[text()='Delete']");
//    private By backBtn = By.linkText("Back");
//
    public void clickUpdateButton(){
        $(updateBtn).click();
    }

}
