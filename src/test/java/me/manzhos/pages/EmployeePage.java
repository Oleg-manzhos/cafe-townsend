package me.manzhos.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class EmployeePage extends BasePage{

    private By createEmployeeBtn = By.linkText("Create");
    private By editEmployeeBtn = By.linkText("Edit");
    private By deleteEmployeeBtn = By.linkText("Delete");
    private By logoutBtn = By.cssSelector("p.main-button");

    public void editEmployeeRecordByEditButton(String fname, String lname){
        findEmployeeByFullName(setFullName(fname, lname)).click();
        $(editEmployeeBtn).click();
    }

    private SelenideElement findEmployeeByFullName(String employeeFullName){
        return $x("//li[contains(text(), '"+employeeFullName+"')]");
    }

    public SelenideElement isEmployeePage() {
        return $(createEmployeeBtn).should(Condition.visible);
    }

    public void initiateLogOut(){
        $(logoutBtn).click();
    }

    public void initiateEmployeeCreation() {
        $(createEmployeeBtn).click();
    }

    public void deleteUserByNames(String fname, String lname){
        findEmployeeByFullName(setFullName(fname, lname)).click();
        $(deleteEmployeeBtn).click();
        WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    public SelenideElement returnUserByName(String fname, String lname){
        return findEmployeeByFullName(setFullName(fname, lname));
    }

    private String setFullName (String fname, String lname){
        return fname +" "+lname;
    }
}
