package me.manzhos.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CreateEmployeePage extends BasePage {

    private CreateUpdateCommonElementsPage commonElements = new CreateUpdateCommonElementsPage();
    private By cancelBtn = By.linkText("Cancel");
    private By addBtn = By.xpath("//button[text()='Add']");

    public boolean isCreateEmployeePage(){
        return $(cancelBtn).exists();
    }

    public void submitEmployee() {
        if ($(addBtn).is(Condition.enabled)) {
            $(addBtn).click();
        }
         else{
             WebDriverRunner.getWebDriver().switchTo().alert().accept();
         }
    }
}
