# cafe-townsend

To download all dependencies, please run
```
mvn clean install
```
To start all tests in Chrome (starts by default), the following command should be executed:
```
mvn clean test
```
*NOTE:* Automation project uses Boni's Garcia Driver Manager that automatically downloads driver version that fits your browser. Please ensure that your firewall/proxy is configured properly to let the driver be downloaded.

## Test Execution

There are several ways to run tests

### IDE way

You can run stories from your IDE.
It is preferably run not feature files by themselves, but rather appropriate runners:
 * AllTestsRunner
 * CreateEmployeeRunner
 * DeleteEmployeeRunner
 * EditEmployeeRunner
 * LoginRunner

All runners contain necessary tags to tight appropriate stories only. 
### Run tests in batch and individually

To run tests in firefox or IE the following parameters should to be passed as command line argument's value:
```
mvn clean test -Dselenide.browser=firefox
```
or
```
mvn clean test -Dselenide.browser=ie
```
Project contains surefire plugin that allows to run tests separately:
```
mvn clean test -Dtest=LoginRunner
```

Any of mentioned classes (runners package) could be run separately using -D*test* parameter.
 
### Running with report

 Report on results of testing could is generated based on Maven Surefire plugin and is driven by standard Cucumber approach.
 To run report generation, it is necessary to start tests using the following command:
```
 mvn clean test -Dtest=AllTestsRunner
```
After execution will be finished, the report will be located here:
[target/surefire-reports/index.html](target/cucumber-report/index.html)

##Selected toolset

Project uses the following set of technologies:
Java 1.8 + Maven + Selenium WebDriver 3 + Selenide 5 + Cucumber 4

Selenium + Cucumber were selected as they are required for current job position. Selenide is an API over Selenium where browser creation and some waiters are implemented.
Java was selected as one of the two possible languages that supports both Selenium and Cucumber pair.
Maven was selected as widely used tool for dependency management and project setup (build phase and reporting phase)   